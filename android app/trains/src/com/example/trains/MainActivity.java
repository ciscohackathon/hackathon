package com.example.trains;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;
import android.webkit.WebViewClient;


public class MainActivity extends Activity {

	//creates the WebView
	private WebView trainSite;


	public void onCreate(Bundle savedInstanceState){
		/**
		 super.onCreate(savedInstanceState);
		 setContentView(R.layout.activity_main);
		 mLoading = (ProgressBar) findViewById(R.id.progressBar1);
		 setContentView(mLoading);
		 */

		super.onCreate(savedInstanceState);
		trainSite = new WebView(this);
		
		//delete next line if we don't need JavaScript.  (Security risk)
		trainSite.getSettings().setJavaScriptEnabled(true); 

		//Defines the WebView
		final Activity mobileView = this;

		trainSite.setWebViewClient(new WebViewClient(){
			@SuppressWarnings("unused") //suppress the warnings for 'failure' (shouldn't be failure, only 'webpage cannot be displayed error')
			public void onRecievedError(WebView view, int errorCode, String description, String failingUrl){
				Toast.makeText(mobileView, description, Toast.LENGTH_SHORT).show();
			}
		});
		//loads the site into the WebView
		trainSite.loadUrl("http://www.harrisonsouth.co.uk/trains");

		//displays the WebView
		setContentView(trainSite);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setContentView(R.layout.activity_main);
	}

	//stops the back button exiting the application
	@Override
	public void onBackPressed() {
		if(trainSite.canGoBack()) {
			trainSite.goBack();
		} else {
			super.onBackPressed();
		}
	}

	//calls method in AppWebViewClient.java to ensure normal http:// do not open with this app
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		return false;
	}
}
